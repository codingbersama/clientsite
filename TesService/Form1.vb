﻿Imports System.Web.Script.Serialization

Public Class Form1
    Private ftools As New FTools

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim param As New Specialized.NameValueCollection
        param.Add("data", TextBox1.Text)
        ftools.sendData("http://service.comoj.com/index.php", param)
        load()
        TextBox1.Text = ""
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load()
    End Sub

    Private Sub load()
        Dim data As String
        data = ftools.getData("http://service.comoj.com/index.php?get")
        Dim json As New JavaScriptSerializer
        Dim jsonOut As RootObject = json.Deserialize(Of RootObject)(data)
        ListBox1.Items.Clear()
        For Each i As Datum In jsonOut.datas
            ListBox1.Items.Add("ID: " & i.id & " => Data: " & i.data)
        Next
    End Sub

    Public Class RootObject
        Public Property datas As List(Of Datum)
    End Class

    Public Class Datum
        Public Property id As String
        Public Property data As String
    End Class

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub
End Class
